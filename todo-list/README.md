# TODO List Leo Costa #
## Gulp 4 + Webpack 4 + Babel + BrowserSync ##

## Description

- Simple TODO list in Vanilla Java Script, the data will be saved in the browser local storage for testing proposes, UI elements created by scratch following the Grant Thornton website design styles.

- JavaScript files used in the project are:
  - src/js/todo/TodoListing.js - manipulate list
  - src/js/todo/ListController.js - Add/delete data
  - src/js/todo/ListsDataSample.js - Data template for testing proposes
  - src/js/todo/UserLogin.js - User Login info for testing proposes

- SCSS files are (BEM methodology):
  - src/scss/components/login-modal.js - Login component style
  - src/scss/components/todo-panel.js - TODO list component style

- HTML5 files are:
  - src/homepage.html - Main page
  - src/includes/components/login-modal.html - Login component
  - src/includes/components/todo-panel.html - TODO list component


-  A compiled version is included in website/dist

- Please contact me hello@leandroscosta.com if needed


## Login details

- User: Ankita / Password: 0000
- User: Peng / Password: 1111


For Live reloading, Browsersync has been used. For ES6 Transpilation, Babel has been used. For SourceMaps, Gulp & Webpack both have been used. Sass/CSS Workflow.

## Setup

- Install [Node](https://nodejs.org/) *_Version 12.18.4_*
- Use *Npm* that comes with Node pre-installed
- Install Gulp globally through `npm install -g gulp@next`
- Install Webpack globally through `npm install -g webpack`
- Clone this project
- Install all [packages](HTML/package.json) with `npm install`

## Usage

- **Build the Project and Serve locally (Development build)** - `npm run dev`. The Development port is `3000`. Build directory is `/dist`
- **Build the Project and Serve locally (Production build)** - `npm start`. The Production port is `8000`. Build directory is `/build`
- **Build the Project without Serving or Watching (Production build)** - `npm run build`. Build directory is `/build`
- **Exporting the Project to zip file** - `npm run export` or `yarn run export`

Important Note: **Don't** run these npm scripts simultaneously.

## Appendix

- **Tooling** - Gulpfile Lives in `gulpfile.js` and Webpack config files live within `webpack` folder.
- **Source Files** - Lives in `website/src` folder
- **Compiled Files for development** - Generated into `website/dist` folder.

## Transpilation

**Babel** is used with Webpack to transpile modern javascript according to the compatibility outlined in .browserslistrc, and allow for es6 module imports.

By default, the contents of _node_modules_ are excluded from babel transpilation. However, any node packages that use ES6+ will also need to be transpiled. To enable this, add the package name (should match the name used in package.json) to the array in /webpack/nodeModulesToTranspile.js.

If this step is not completed, you'll see an error when attempting a production build that's similar to the following:
```bash
gulpUglifyError: unable to minify JavaScript
Caused by: SyntaxError: Unexpected token: keyword «const»
File: /Users/ronan/Sites/kooba-frontend-base/main.js 
```

