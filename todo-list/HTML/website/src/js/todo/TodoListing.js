import ListController from './ListController';
import UsersLogin from './UsersLogin';

export default class TodoListing {
    constructor() {
        this.user = '';
        this.listItems = [];
        this.usersLogin = new UsersLogin();
        this.listController = new ListController();
        this.buttonSubmit = document.querySelector('#button-submit');
        this.buttonAddItem = document.querySelector('#add-item');
        this.buttonLogout = document.querySelector('#logout');
        this.userField = document.querySelector('#user');
        this.passwordField = document.querySelector('#password');
        this.newTodoItemField = document.querySelector('#new-item');
        this.loginAlert = document.querySelector('.login-modal__alert');
    }

    checkAccessDetails() {
        if (this.usersLogin.usersLoginInfo.length > 0) {
            this.usersLogin.usersLoginInfo.forEach((userLogin) => {
                if ((userLogin.user === this.userField.value) && (userLogin.password === this.passwordField.value) ) {
                    this.setUser(this.userField.value);
                    this.loadTodoList();
                    this.refreshItemsInHTMLMarkup();
                    this.newTodoItemField.focus();
                } else {
                    this.updateAlertMessage();
                }
            })
        }
    }

    setUser(user) {
        this.user = user;
    }

    loadTodoList() {
        this.hideLoginModal();
        this.listController.init();
    }

    hideLoginModal() {
        const loginModal = document.querySelector('.login-modal');
        loginModal.classList.remove('active');
    }

    showLoginModal() {
        const loginModal = document.querySelector('.login-modal');
        loginModal.classList.add('active');

        this.userField.value = '';
        this.passwordField.value = '';

        this.allowButton(this.userField, this.buttonSubmit);
        this.checkAccessDetails();
    }

    updateAlertMessage() {
        if ((this.userField.value !== '') && (this.passwordField.value !== '')) {
            this.showLoginErrorAlert();
        } else {
            this.hideLoginErrorAlert();
        }
    }

    showLoginErrorAlert() {
        if(!(this.loginAlert.classList.contains('active'))) {
            this.loginAlert.classList.add('active')
        }
    }

    hideLoginErrorAlert() {
        if(this.loginAlert.classList.contains('active')) {
            this.loginAlert.classList.remove('active');
        }
    }

    allowButton(field, button) {
        if(field.value !== "") {
            button.classList.add('active');
        } else {
            if(button.classList.contains('active')) {
                button.classList.remove('active');
            }
        }
    }

    addTodoItem(todoItemValue) {
        this.listController.addTodoList(this.user, todoItemValue);
        this.refreshItemsInHTMLMarkup();
        this.newTodoItemField.focus();
    }

    getUserList() {
        const todoLists = this.listController.todoLists;

        if (todoLists.length > 0) {
            todoLists.forEach((todoList, index) => {
                let listUserName = todoList.user;

                if (listUserName === this.user) {
                    this.listItems = todoList.items;
                }
            })
        }
    }

    refreshItemsInHTMLMarkup() {
        this.getUserList();

        const container = document.querySelector('.todo-panel__items');
        const todoTitle = document.querySelector('.todo-panel__list-title');

        container.innerHTML = '';
        todoTitle.innerHTML = `${this.user} list:`;
        this.newTodoItemField.value = '';

        if (this.listItems.length > 0) {
            this.listItems.forEach((listItem, index) => {
                container.appendChild(this.renderItems(listItem, index));
            })
        }

        this.initiateCloseEventListeners();
    }

    renderItems(item, index) {
        const itemToRender = document.createRange().createContextualFragment(`
               <li class="todo-panel__todo-item">
                        <button id="close-item-${index}" class="todo-panel__remove-button" aria-label="Remove item"><i class="icon-close todo-panel__todo-icon"></i></button>
                        <p class="todo-panel__item-text">${item}</p>
               </li>
        `);
        return itemToRender;
    }

    initiateCloseEventListeners() {
        if (this.listItems.length > 0) {
            this.listItems.forEach((item, index) => {
                const closeButton = document.querySelector(`#close-item-${index}`);
                closeButton.addEventListener('click', e => this.removeItemOnClick(e, index));
            })
        }
    }

    removeItemOnClick(e, index) {
        const closeButton = document.querySelector(`#close-item-${index}`);
        const itemText = closeButton.nextElementSibling.innerHTML;
        this.listController.removeTodoList(this.user, itemText);

        this.refreshItemsInHTMLMarkup();
    }

    onPasswordEnter(e) {
        if (e.key === 'Enter') {
            this.checkAccessDetails();
        }
    }

    onItemEnter(e) {
        if (e.key === 'Enter') {
            this.addTodoItem(this.newTodoItemField.value);
        }
    }

    registerListeners() {
        this.userField.addEventListener('input', e => this.allowButton(this.userField, this.buttonSubmit));
        this.passwordField.addEventListener('keypress', e => this.onPasswordEnter(e));
        this.buttonSubmit.addEventListener('click', e => this.checkAccessDetails());
        this.newTodoItemField.addEventListener('input', e => this.allowButton(this.newTodoItemField, this.buttonAddItem));
        this.newTodoItemField.addEventListener('keypress', e => this.onItemEnter(e));
        this.buttonAddItem.addEventListener('click', e => this.addTodoItem(this.newTodoItemField.value));
        this.buttonLogout.addEventListener('click', e => this.showLoginModal());
    }

    init() {
        this.registerListeners();
    }
}