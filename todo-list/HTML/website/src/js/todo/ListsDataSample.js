export default class ListsDataSample {

    //Data can be received via API from BE
    constructor() {
        this.dataSample = [
            {
                user: 'Ankita',
                items: []
            },

            {
                user: 'Peng',
                items: []
            }
        ];
    }
}