import ListsDataSample from './ListsDataSample';

export default class ListController {
    constructor() {
        this.listdDataSample = new ListsDataSample();
        this.todoLists = this.listdDataSample.dataSample;
    }

    initiateTodoItems() {
        const localStorageTodoList = localStorage.getItem('todoLists');

        if(localStorageTodoList !== null) {
            this.updateTodoList(localStorageTodoList);
        }
    }

    updateTodoList(lists) {
        this.todoLists = JSON.parse(lists);
        this.saveListInDataBase();
    }

    saveListInDataBase(){
        const myJSON = JSON.stringify(this.todoLists);
        localStorage.setItem('todoLists', myJSON);
    }

    addTodoList(user, todoItemValue) {
        if (this.todoLists.length > 0) {
            this.todoLists.forEach((todoList, index) => {
                let listUserName = todoList.user;

                if (listUserName === user) {
                    todoList.items.push(todoItemValue);
                }
            })
        }

        this.saveListInDataBase();
    }


    removeTodoList(user, todoItemValue) {
        if (this.todoLists.length > 0) {
            this.todoLists.forEach((todoList, index) => {
                if(todoList.user === user) {
                    const userItems = todoList.items;
                    const indexOfItem = userItems.indexOf(todoItemValue);

                    const newUserItems = userItems;
                    newUserItems.splice(indexOfItem, 1);

                    todoList.items = newUserItems;
                }
            })
        }

        this.saveListInDataBase();
    }

    init() {
        this.initiateTodoItems();
    }
}
