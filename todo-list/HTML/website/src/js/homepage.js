import TodoListing from './todo/TodoListing';

const initiateTodoList = () => {
    const todoListing = new TodoListing();
    todoListing.init();
};

window.addEventListener('DOMContentLoaded', () => {
    initiateTodoList();
});