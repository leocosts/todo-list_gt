/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"homepage": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./website/src/js/homepage.js","vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./website/src/js/homepage.js":
/*!************************************!*\
  !*** ./website/src/js/homepage.js ***!
  \************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _todo_TodoListing__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./todo/TodoListing */ "./website/src/js/todo/TodoListing.js");


var initiateTodoList = function initiateTodoList() {
  var todoListing = new _todo_TodoListing__WEBPACK_IMPORTED_MODULE_0__["default"]();
  todoListing.init();
};

window.addEventListener('DOMContentLoaded', function () {
  initiateTodoList();
});

/***/ }),

/***/ "./website/src/js/todo/ListController.js":
/*!***********************************************!*\
  !*** ./website/src/js/todo/ListController.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ListController; });
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.index-of */ "./node_modules/core-js/modules/es.array.index-of.js");
/* harmony import */ var core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_array_splice__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.array.splice */ "./node_modules/core-js/modules/es.array.splice.js");
/* harmony import */ var core_js_modules_es_array_splice__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_splice__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ListsDataSample__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ListsDataSample */ "./website/src/js/todo/ListsDataSample.js");





function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var ListController = /*#__PURE__*/function () {
  function ListController() {
    _classCallCheck(this, ListController);

    this.listdDataSample = new _ListsDataSample__WEBPACK_IMPORTED_MODULE_4__["default"]();
    this.todoLists = this.listdDataSample.dataSample;
  }

  _createClass(ListController, [{
    key: "initiateTodoItems",
    value: function initiateTodoItems() {
      var localStorageTodoList = localStorage.getItem('todoLists');

      if (localStorageTodoList !== null) {
        this.updateTodoList(localStorageTodoList);
      }
    }
  }, {
    key: "updateTodoList",
    value: function updateTodoList(lists) {
      this.todoLists = JSON.parse(lists);
      this.saveListInDataBase();
    }
  }, {
    key: "saveListInDataBase",
    value: function saveListInDataBase() {
      var myJSON = JSON.stringify(this.todoLists);
      localStorage.setItem('todoLists', myJSON);
    }
  }, {
    key: "addTodoList",
    value: function addTodoList(user, todoItemValue) {
      if (this.todoLists.length > 0) {
        this.todoLists.forEach(function (todoList, index) {
          var listUserName = todoList.user;

          if (listUserName === user) {
            todoList.items.push(todoItemValue);
          }
        });
      }

      this.saveListInDataBase();
    }
  }, {
    key: "removeTodoList",
    value: function removeTodoList(user, todoItemValue) {
      if (this.todoLists.length > 0) {
        this.todoLists.forEach(function (todoList, index) {
          if (todoList.user === user) {
            var userItems = todoList.items;
            var indexOfItem = userItems.indexOf(todoItemValue);
            var newUserItems = userItems;
            newUserItems.splice(indexOfItem, 1);
            todoList.items = newUserItems;
          }
        });
      }

      this.saveListInDataBase();
    }
  }, {
    key: "init",
    value: function init() {
      this.initiateTodoItems();
    }
  }]);

  return ListController;
}();



/***/ }),

/***/ "./website/src/js/todo/ListsDataSample.js":
/*!************************************************!*\
  !*** ./website/src/js/todo/ListsDataSample.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ListsDataSample; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ListsDataSample = //Data can be received via API from BE
function ListsDataSample() {
  _classCallCheck(this, ListsDataSample);

  this.dataSample = [{
    user: 'Ankita',
    items: []
  }, {
    user: 'Peng',
    items: []
  }];
};



/***/ }),

/***/ "./website/src/js/todo/TodoListing.js":
/*!********************************************!*\
  !*** ./website/src/js/todo/TodoListing.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return TodoListing; });
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.concat */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ListController__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ListController */ "./website/src/js/todo/ListController.js");
/* harmony import */ var _UsersLogin__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./UsersLogin */ "./website/src/js/todo/UsersLogin.js");




function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }




var TodoListing = /*#__PURE__*/function () {
  function TodoListing() {
    _classCallCheck(this, TodoListing);

    this.user = '';
    this.listItems = [];
    this.usersLogin = new _UsersLogin__WEBPACK_IMPORTED_MODULE_4__["default"]();
    this.listController = new _ListController__WEBPACK_IMPORTED_MODULE_3__["default"]();
    this.buttonSubmit = document.querySelector('#button-submit');
    this.buttonAddItem = document.querySelector('#add-item');
    this.buttonLogout = document.querySelector('#logout');
    this.userField = document.querySelector('#user');
    this.passwordField = document.querySelector('#password');
    this.newTodoItemField = document.querySelector('#new-item');
    this.loginAlert = document.querySelector('.login-modal__alert');
  }

  _createClass(TodoListing, [{
    key: "checkAccessDetails",
    value: function checkAccessDetails() {
      var _this = this;

      if (this.usersLogin.usersLoginInfo.length > 0) {
        this.usersLogin.usersLoginInfo.forEach(function (userLogin) {
          if (userLogin.user === _this.userField.value && userLogin.password === _this.passwordField.value) {
            _this.setUser(_this.userField.value);

            _this.loadTodoList();

            _this.refreshItemsInHTMLMarkup();

            _this.newTodoItemField.focus();
          } else {
            _this.updateAlertMessage();
          }
        });
      }
    }
  }, {
    key: "setUser",
    value: function setUser(user) {
      this.user = user;
    }
  }, {
    key: "loadTodoList",
    value: function loadTodoList() {
      this.hideLoginModal();
      this.listController.init();
    }
  }, {
    key: "hideLoginModal",
    value: function hideLoginModal() {
      var loginModal = document.querySelector('.login-modal');
      loginModal.classList.remove('active');
    }
  }, {
    key: "showLoginModal",
    value: function showLoginModal() {
      var loginModal = document.querySelector('.login-modal');
      loginModal.classList.add('active');
      this.userField.value = '';
      this.passwordField.value = '';
      this.allowButton(this.userField, this.buttonSubmit);
      this.checkAccessDetails();
    }
  }, {
    key: "updateAlertMessage",
    value: function updateAlertMessage() {
      if (this.userField.value !== '' && this.passwordField.value !== '') {
        this.showLoginErrorAlert();
      } else {
        this.hideLoginErrorAlert();
      }
    }
  }, {
    key: "showLoginErrorAlert",
    value: function showLoginErrorAlert() {
      if (!this.loginAlert.classList.contains('active')) {
        this.loginAlert.classList.add('active');
      }
    }
  }, {
    key: "hideLoginErrorAlert",
    value: function hideLoginErrorAlert() {
      if (this.loginAlert.classList.contains('active')) {
        this.loginAlert.classList.remove('active');
      }
    }
  }, {
    key: "allowButton",
    value: function allowButton(field, button) {
      if (field.value !== "") {
        button.classList.add('active');
      } else {
        if (button.classList.contains('active')) {
          button.classList.remove('active');
        }
      }
    }
  }, {
    key: "addTodoItem",
    value: function addTodoItem(todoItemValue) {
      this.listController.addTodoList(this.user, todoItemValue);
      this.refreshItemsInHTMLMarkup();
      this.newTodoItemField.focus();
    }
  }, {
    key: "getUserList",
    value: function getUserList() {
      var _this2 = this;

      var todoLists = this.listController.todoLists;

      if (todoLists.length > 0) {
        todoLists.forEach(function (todoList, index) {
          var listUserName = todoList.user;

          if (listUserName === _this2.user) {
            _this2.listItems = todoList.items;
          }
        });
      }
    }
  }, {
    key: "refreshItemsInHTMLMarkup",
    value: function refreshItemsInHTMLMarkup() {
      var _this3 = this;

      this.getUserList();
      var container = document.querySelector('.todo-panel__items');
      var todoTitle = document.querySelector('.todo-panel__list-title');
      container.innerHTML = '';
      todoTitle.innerHTML = "".concat(this.user, " list:");
      this.newTodoItemField.value = '';

      if (this.listItems.length > 0) {
        this.listItems.forEach(function (listItem, index) {
          container.appendChild(_this3.renderItems(listItem, index));
        });
      }

      this.initiateCloseEventListeners();
    }
  }, {
    key: "renderItems",
    value: function renderItems(item, index) {
      var itemToRender = document.createRange().createContextualFragment("\n               <li class=\"todo-panel__todo-item\">\n                        <button id=\"close-item-".concat(index, "\" class=\"todo-panel__remove-button\" aria-label=\"Remove item\"><i class=\"icon-close todo-panel__todo-icon\"></i></button>\n                        <p class=\"todo-panel__item-text\">").concat(item, "</p>\n               </li>\n        "));
      return itemToRender;
    }
  }, {
    key: "initiateCloseEventListeners",
    value: function initiateCloseEventListeners() {
      var _this4 = this;

      if (this.listItems.length > 0) {
        this.listItems.forEach(function (item, index) {
          var closeButton = document.querySelector("#close-item-".concat(index));
          closeButton.addEventListener('click', function (e) {
            return _this4.removeItemOnClick(e, index);
          });
        });
      }
    }
  }, {
    key: "removeItemOnClick",
    value: function removeItemOnClick(e, index) {
      var closeButton = document.querySelector("#close-item-".concat(index));
      var itemText = closeButton.nextElementSibling.innerHTML;
      this.listController.removeTodoList(this.user, itemText);
      this.refreshItemsInHTMLMarkup();
    }
  }, {
    key: "onPasswordEnter",
    value: function onPasswordEnter(e) {
      if (e.key === 'Enter') {
        this.checkAccessDetails();
      }
    }
  }, {
    key: "onItemEnter",
    value: function onItemEnter(e) {
      if (e.key === 'Enter') {
        this.addTodoItem(this.newTodoItemField.value);
      }
    }
  }, {
    key: "registerListeners",
    value: function registerListeners() {
      var _this5 = this;

      this.userField.addEventListener('input', function (e) {
        return _this5.allowButton(_this5.userField, _this5.buttonSubmit);
      });
      this.passwordField.addEventListener('keypress', function (e) {
        return _this5.onPasswordEnter(e);
      });
      this.buttonSubmit.addEventListener('click', function (e) {
        return _this5.checkAccessDetails();
      });
      this.newTodoItemField.addEventListener('input', function (e) {
        return _this5.allowButton(_this5.newTodoItemField, _this5.buttonAddItem);
      });
      this.newTodoItemField.addEventListener('keypress', function (e) {
        return _this5.onItemEnter(e);
      });
      this.buttonAddItem.addEventListener('click', function (e) {
        return _this5.addTodoItem(_this5.newTodoItemField.value);
      });
      this.buttonLogout.addEventListener('click', function (e) {
        return _this5.showLoginModal();
      });
    }
  }, {
    key: "init",
    value: function init() {
      this.registerListeners();
    }
  }]);

  return TodoListing;
}();



/***/ }),

/***/ "./website/src/js/todo/UsersLogin.js":
/*!*******************************************!*\
  !*** ./website/src/js/todo/UsersLogin.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UsersLogin; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UsersLogin = //Data can be received via API from BE
function UsersLogin() {
  _classCallCheck(this, UsersLogin);

  this.usersLoginInfo = [{
    user: 'Ankita',
    password: '0000'
  }, {
    user: 'Peng',
    password: '1111'
  }];
};



/***/ })

/******/ });
//# sourceMappingURL=homepage.js.map
